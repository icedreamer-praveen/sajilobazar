from django import forms
from ecomapp.models import Admin
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

class SellerForm(forms.ModelForm):
	class Meta:
		model = Admin
		fields = ['first_name', 'middle_name', 'last_name', 'email',
			 'address', 'image', 'mobile', 'location']

	def clean_email(self):
		email = self.cleaned_data.get("email")
		if User.objects.filter(email=email).exists():
			raise ValidationError(
				"Seller with this email address already exist"
			)
		return email

	def clean_confirm_password(self):
		password = self.cleaned_data.get("password")
		confirm_password = self.cleaned_data.get("confirm_password")
		if password != confirm_password:
			raise ValidationError(
				"Password did not match"
			)
		return confirm_password


class SellerForgotPasswordForm(forms.Form):
	email = forms.CharField(widget=forms.EmailInput(attrs={
		'class' : 'form-control',
		"placeholder":"Enter the email used in seller account ..."
	}))

	def clean_email(self):
		e = self.cleaned_data.get("email")
		if User.objects.filter(email = e).exists():
			pass
		else:
			raise forms.ValidationError("Seller with this email does not exists...")
		return e

class SellerPasswordResetForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : "form-control floating",
        'autocomplete' : "new password",
    }))
    confirm_new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : 'form-contrl floating',
        'autocomplete' : 'confirm new password'
    }))

    def clean_confirm_new_password(self):
        new_password = self.cleaned_data.get("new_password")
        confirm_new_password = self.cleaned_data.get("confirm_new_password")
        if new_password != confirm_new_password:
            raise ValidationError(
                "Password did not match!"
            )

        return confirm_new_password
