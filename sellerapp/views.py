from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.views.generic import CreateView
from django.urls import reverse_lazy, reverse
from django.views.generic.edit import FormView
from ecomapp.models import Admin
from ecomapp.utils import password_reset_token
from .forms import SellerForm, SellerForgotPasswordForm, SellerPasswordResetForm


class SellerRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		user = request.user
		if user.is_authenticated and user.groups.filter(name="Seller").exists():
			pass
		else:
			return redirect("/customer-login/")

		return super().dispathc(request, *args, **kwargs)


class SellerRegistrationView(CreateView):
    template_name = 'seller/sellerregistration.html'
    form_class = SellerForm
    success_url = reverse_lazy('ecomapp:home')

    # def form_valid(self, form):
    #     email = form.cleaned_data.get("email")
    #     confirm_password = form.cleaned_data.get("confirm_password")
    #     user = User.objects.create_user(email, email, confirm_password)
    #     form.instance.user = user
    #     return super().form_valid(form)

class SellerForgotPassword(FormView):
    template_name = "seller/sellerforgotpassword.html"
    form_class = SellerForgotPasswordForm
    success_url = '/seller-forgot-password/?m=s'

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        url = self.request.META["HTTP_HOST"]
        seller = User.objects.get(email = email)
        # user = seller.email
        text_content = "Please click the link below to reset your password"
        html_content = ' http://'+ url + "/password-reset/" + email + \
            "/" + password_reset_token.make_token(seller) + "/"
        send_mail(
            "Password Reset Link | Sajilobazar",
            text_content+html_content,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,


        )
        return super().form_valid(form)

class SellerPasswordResetView(FormView):
    template_name = "seller/sellerpasswordresert.html"
    form_class = SellerPasswordResetForm
    success_url = '/admin-login/'

    def dispatch(self, request, *args, **kwargs):
        email = self.kwargs.get("email")
        seller = User.objects.get(email = email)
        token = self.kwargs.get("token")
        if seller is not None and password_reset_token.check_token(seller, token):
            pass
        else:
            return redirect(reverse("sellerforgotpassword") + "?m=e")
        return super().dispatch(request, *args, **kwargs)


    def form_valid(self, form):
        password = form.cleaned_data['new_password']
        email = self.kwargs.get("email")
        seller = User.objects.get(email = email)
        seller.set_password(password)
        seller.save()
        return super().form_valid(form)
