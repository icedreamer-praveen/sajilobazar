from django.urls import path
from .views import *

app_name = "sellerapp"

urlpatterns = [
    path("seller-registration/", SellerRegistrationView.as_view(), name="sellerregistration"),
    path("seller-forgot-password/", SellerForgotPassword.as_view(), name="sellerforgotpassword"),
    path("seller-password-reset/<email>/<token>/", SellerPasswordResetView.as_view(),name="sellerpasswordreset"),
]
