from django import forms
from django.forms import fields
from .models import Images, Order, Customer, Admin, Product
from django.contrib.auth.models import User

class CheckOutForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = ["ordered_by","shipping_address","mobile","email", "payment_method"]
		widgets = {
			'ordered_by' : forms.TextInput(attrs={
				'class' : 'billing-address-name','placeholder' : "Your Name"
				}),
			'shipping_address' : forms.TextInput(attrs={
				'placeholder' : "Shiping Address"
				}),
			'mobile' : forms.TextInput(attrs={
				'placeholder': "Mobile Number"
				}),
			'email' : forms.TextInput(attrs={
				'placeholder' : 'Email',
				}),

		}

class CustomerRegistrationForm(forms.ModelForm):
		password = forms.CharField(widget=forms.PasswordInput())
		email = forms.CharField(widget=forms.EmailInput())
		class Meta:
				model = Customer
				fields = ["password", 'mobile',"full_name","address"]
				widgets = {
				'password' : forms.TextInput(attrs={
						'billing-address-name':'billing-address-name','placeholder' : "Password",
						'class' : 'form-control'
						}),
				'email' : forms.TextInput(attrs={
						'placeholder': "Email"
						}),
				'full_name' : forms.TextInput(attrs={
						'placeholder': "Full Name"
						}),                                        
				'address' : forms.TextInput(attrs={
						'placeholder': "Address"
						}),
				}
		def clean_username(self):
				uname = self.cleaned_data.get("username")
				if User.objects.filter(username=uname).exists():
						raise forms.ValidationError("Customer with this user name already exit")

				return uname




class CustomerLoginForm(forms.Form):
		username = forms.CharField(widget=forms.TextInput())
		password = forms.CharField(widget=forms.PasswordInput())


class AdminLoginForm(forms.Form):
	username = forms.CharField(widget=forms.TextInput())
	password = forms.CharField(widget=forms.PasswordInput())

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['username'].widget.attrs.update({
			'class' : 'billing-address-name','placeholder' : "User Name"
		})
		self.fields['password'].widget.attrs.update({
			'class':'lock', 'placeholder' : "Password"
		})

	def clean(self):
		cleaned_data = super().clean()
		username = self.cleaned_data.get("username")
		password = self.cleaned_data.get("password")
		user = User.objects.filter(username=username, is_active=True).first()
		if user == None or not user.check_password(password):
			raise forms.ValidationError("Incorrect username or password")
		return self.cleaned_data

class ImageForm(forms.ModelForm):
		class Meta:
				model = Images
				fields = ['image']
				

class ProductForm(forms.ModelForm):     
	class Meta:
		model = Product
		fields = ['title','slug','category','marked_price','selling_price','description','warranty','return_policy']
		widgets ={
				"title": forms.TextInput(attrs={
						"class":"form-control",
						"placeholder":"Enter the Product title here.."
						}),
				"slug": forms.TextInput(attrs={
						"class":"form-control",
						"placeholder":"Enter slug.."
						}),
				"category": forms.Select(attrs={
						"class":"form-control",
						}),
				"image": forms.ClearableFileInput(attrs={
						"class":"form-control"
						}),
				"marked_price": forms.NumberInput(attrs={
						"class":"form-control",
						"placeholder":"Marked Price of the product .."
						}),                        
				"selling_price": forms.NumberInput(attrs={
						"class":"form-control",
						"placeholder":"Selling Price of the product .."
						}),
				"description": forms.Textarea(attrs={
						"class":"form-control",
						"placeholder":"description of product...",
						"rows":8,
						}),
				"slug": forms.TextInput(attrs={
						"class":"form-control",
						"placeholder":"Enter slug.."
						}),
		}




class ForgotPasswordForm(forms.Form):
	email = forms.CharField(widget=forms.EmailInput(attrs={
		"class":"form-control",
		"placeholder":"Enter the email used in customer account ..."
		}),)


	def clean_email(self):
		e = self.cleaned_data.get("email")
		if User.objects.filter(email=e).exists():
			pass
		else:
			raise forms.ValidationError("Customer with this email does not exists..")
		return e
		email = forms.CharField(widget=forms.EmailInput(attrs={
				"class":"form-control",
				"placeholder":"Enter the email used in customer account ..."
				}),)


		def clean_email(self):
				e = self.cleaned_data.get("email")
				if Customer.objects.filter(user__email=e).exists():
						pass
				else:
						raise forms.ValidationError("Customer with this email does not exists..")
				return e
class PasswordResetForm(forms.Form):
		new_password = forms.CharField(widget=forms.PasswordInput(attrs={
				"class":"form-control",
				"autocomplete": "new-password",
				"placeholder": "Enter New Password",                
				}),label="New Password")
		confirm_new_password = forms.CharField(widget=forms.PasswordInput(attrs={
				"class":"form-control",
				"autocomplete": "new-password",
				"placeholder": "Enter Confirm New Password",                
				}),label="Confirm New Password")

		def clean_confirm_new_password(self):
				new_password = self.cleaned_data.get("new_password")
				confirm_new_password = self.cleaned_data.get("confirm_new_password")
				if new_password != confirm_new_password:
						raise forms.ValidationError("New Password did not match!")

				return confirm_new_password