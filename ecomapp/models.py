from django.db import models
from django.contrib.auth.models import User,Group
from PIL import Image
from django.db.models.fields import related_descriptors
from django.utils import timezone
from ckeditor.fields import RichTextField

class DateTimeModel(models.Model):
	created_at = models.DateTimeField(
		auto_now_add=True,
		auto_now=False,
	)
	updated_at = models.DateTimeField(
		auto_now_add=False,
		auto_now=True,
	)
	deleted_at = models.DateTimeField(null=True, blank=True)

	class Meta:
		abstract = True

	def delete(self, hard=False):
		if not hard:
			self.deleted_at = timezone.now()
			super().save()
		else:
			super().delete()

SELER_STATUS = (
	('Accepted','Accepted'),
	('Pending', 'Pending'),
	('Blocked', 'Blocked')
)

class Images(models.Model):
	image = models.ImageField(upload_to = 'images')

	class Meta:
		verbose_name = ("Image")
		verbose_name_plural = ("Images")

class Admin(DateTimeModel, User):
	middle_name 	= models.CharField(max_length=200, null=True, blank=True)
	address 	= models.CharField(max_length=300)
	image 		= models.ImageField(upload_to='admin')
	mobile		= models.CharField(max_length=10)
	location = models.CharField(max_length=50)
	status = models.CharField(max_length=50, default="Pending", choices=SELER_STATUS)
	is_seller = models.BooleanField(default=True)

	def save(self, *args, **kwargs):
		self.username = self.email
		return super().save(*args, **kwargs)
		

	def __str__(self):
		return self.username


class Customer(DateTimeModel):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	full_name 	= models.CharField(max_length=200)
	address 	= models.CharField(max_length=300, null=True, blank=True)
	mobile = models.CharField(max_length=10)
	is_customer = models.BooleanField(default=True)


	def __str__(self):
		return self.mobile

class Category(DateTimeModel):
	title 	= models.CharField(max_length=300)
	parent = models.ForeignKey('self', related_name='sub_category', on_delete=models.CASCADE, null=True, blank=True)
	slug 	= models.SlugField(unique=True)

	def __str__(self):
		full_path=[self.title]
		k=self.parent
		while k is not None:
			full_path.append(k.title)
			k = k.parent
		return '->'.join(full_path[::-1])



class Product(DateTimeModel):
	seller 		= models.ForeignKey(Admin, on_delete=models.CASCADE)
	title 		= models.CharField(max_length=300)
	slug 		= models.SlugField(unique=True)
	category 	= models.ForeignKey(Category, on_delete = models.CASCADE)
	image 		= models.ManyToManyField(Images)
	marked_price= models.PositiveIntegerField()
	selling_price=models.PositiveIntegerField()
	description = RichTextField()
	warranty 	= models.CharField(max_length=400, null=True, blank=True)
	return_policy=models.CharField(max_length=300, null=True, blank=True)
	view_count 	= models.PositiveIntegerField(default=0)


	def __str__(self):
		return self.title


class Cart(DateTimeModel):
	customer 	= models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
	total 		= models.PositiveIntegerField(default=0)
	created_at 	= models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "Cart: " + str(self.id)

class CartProduct(DateTimeModel):
	cart 	= models.ForeignKey(Cart, on_delete = models.CASCADE)
	product = models.ForeignKey(Product, on_delete = models.CASCADE)
	rate 	= models.PositiveIntegerField()
	quantity= models.PositiveIntegerField()
	subtotal= models.PositiveIntegerField()

	def __str__(self):
		return "Cart: " + str(self.cart.id) + "   CartProduct: " + str(self.id)


ORDER_STATUS = (
	('Order Received', 'Order Received'),
	('Order Processing', 'Order Processing'),
	('On the way','On the way'),
	('Order Completed', 'Order Completed'),
	('Order Canceled', 'Order Canceled'),
	)

METHOD = {
	("Cash On Delivery", "Cash On Delivery"),
	("Khalti", "Khalti"),
}



class Order(DateTimeModel):
	cart 			= models.OneToOneField(Cart, on_delete=models.CASCADE)
	ordered_by		= models.CharField(max_length=300)
	shipping_address= models.CharField(max_length=200)
	mobile 			= models.CharField(max_length=10)
	email 			= models.EmailField(null=True)
	subtotal		= models.PositiveIntegerField()
	discount 		= models.PositiveIntegerField()
	total			= models.PositiveIntegerField()
	order_status	= models.CharField(max_length=50, choices=ORDER_STATUS)
	created_at		= models.DateTimeField(auto_now_add=True)
	payment_method = models.CharField(
		max_length=20, choices=METHOD, default="Cash On Delivery")
	payment_completed = models.BooleanField(
		default=False, null=True, blank=True) 
	
	def __str__(self):
		return "Order: " + str(self.id)



class Review(DateTimeModel):
	user = models.ForeignKey(Customer, on_delete= models.CASCADE)
	product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
	rate = models.PositiveIntegerField(default=0)
	description = models.TextField()

	def __str__(self):
		return self.product.title
	


