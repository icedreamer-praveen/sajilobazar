from django.urls import path
from .views import *

app_name ="ecomapp"

urlpatterns =[
		path('', HomeView.as_view(), name="home"),
		path('about/', AboutView.as_view(), name="about"),
        path("contact/", ContactView.as_view(), name="contact"),
		path('all-products/', AllProductsView.as_view(), name="allproducts"),
		path('product-detail-<slug:slug>/', ProductDetailView.as_view(), name="product-detail"),

		path('add-to-cart/<int:pro_id>/', AddToCartView.as_view(), name='addtocart'),
		path('my-cart/', MycartView.as_view(), name="mycart"),
		path('manage-cart/<int:cp_id>/', ManageCartView.as_view(), name='namagecart'),
		path('empty-cart/', EmptyCartView.as_view(), name='emptycart'),
		path('check-out/', CheckOutView.as_view(), name='checkout'),
		path('khalti-request/', KhaltiRequestView.as_view(), name='khaltirequest'),
		path('customer-registration/', CustomerRegistrationView.as_view(), name="customerregistration"),
    	path("user-name-checker/", UserNameCheckerView.as_view(), name="usernamechecker"),
    	path("customer-logout/", CustomerLogoutView.as_view(), name="customerlogout"),
    	path("customer-login/", CustomerLoginView.as_view(), name="customerlogin"),
    	path("c-login/", CutomLoginView.as_view(), name="c-login"),
    	path("customer-profile/", CustomerProfileView.as_view(), name="customerprofile"),
    	path("customer-order-detail-<int:pk>/", CustomerOrderDetailView.as_view(), name="customerorderdetail"),
    	path("serch/", SearchView.as_view(), name="search"),
        path("forgot-password/", ForgotPasswordView.as_view(), name="forgot_password"),
        path("password-reset/<email>/<token>/", PasswordResetView.as_view(), name="password_reset"),
        path("product/<slug:slug>/lists/", CategorisedProductView.as_view(), name="product_by_cat"),

    	#admin
    	path("admin-dashboard/", AdminHomeView.as_view(), name="adminhome"),
    	path("admin-login/", AdminLoginView.as_view(), name="adminlogin"),
		path("logout", LogoutView.as_view(), name="logout"),
    	path("admin-dashboard/pending-orders/", AdminPendingOrderView.as_view(), name="amdin_pending_orders"),
    	path("admin-dashboard/order-<int:pk>/", AdminOrderDetailView.as_view(), name="admin_order_detail"),
        path("admin-dashboard/orderlist/", AdminOrderListView.as_view(), name="amdin_orderlist"),
    	path("admin-dashboard/orderlist-<slug:slug>/", AdminOrderByStatusView.as_view(), name="amdin_order_by_status"),
        path("admin-dashboard/order-status-<int:pk>/", AdminOrderStatusChangeView.as_view(), name="admin_order_status_change"),
    	path("admin-dashboard/product-list/", AdminProductListView.as_view(), name="admin_product_list"),
        path("admin-dashboard/product-detail-<int:pk>/", AdminProductDetailView.as_view(), name="admin_product_detail"),
    	path("admin-dashboard/product-create/", AdminProductCreateView.as_view(), name="admin_product_create"),
		path("admin-dashboard/product-update-<int:pk>/", AdminProductUpdateView.as_view(), name="admin_product_update"),
		path("admin-dashboard/product-delete-<int:pk>/", AdminProductDeleteView.as_view(), name="admin_product_delete"),

		# multiple image url 
		path("multiple/image/", MultipleImageCreateView.as_view(),name="multipleimage"),

		#Review url
		path("product/<int:product_pk>/review/", ProductReview.as_view(), name="product-review"),





]