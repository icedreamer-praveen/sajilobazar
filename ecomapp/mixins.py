from django.shortcuts import get_object_or_404, render, redirect

from ecomapp.models import Cart, Customer, Admin




class AdminLoginRequireMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user:
			if request.user.is_authenticated and Admin.objects.get(username=request.user, is_seller=True):
				pass
			else:
				return redirect('/admin-login/')

		return super().dispatch(request, *args, **kwargs)



class EcomMixin(object):
	def dispatch(self, request, *args, **kwargs):
		cart_id = request.session.get("cart_id")
		if cart_id:
			cart_obj = Cart.objects.get(id=cart_id)
			if request.user:
				if request.user.is_authenticated and Customer.objects.get(user=request.user, is_customer=True):
					cart_obj.customer =Customer.objects.get(user=request.user, is_customer=True)
					cart_obj.save()

		return super().dispatch(request, *args, **kwargs)