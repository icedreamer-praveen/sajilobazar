from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import TemplateView, CreateView, FormView, DetailView, ListView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.generic.edit import DeleteView, UpdateView
from .models import *
from .forms import *
from .utils import password_reset_token
from django.core.paginator import Paginator
from django.core.mail import send_mail
from django.contrib import messages

from django.http import HttpRequest, request
from django.views.generic import View
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.conf import settings
from dashboard.mixins import GetDeleteMixin, NonDeletedListMixin

from ecomapp.mixins import EcomMixin, AdminLoginRequireMixin

class AdminHomeView(AdminLoginRequireMixin, TemplateView):
	template_name = 'admin/adminhome.html'

	def get_context_data(self, *args, **kwargs):
		context = super().get_context_data(**kwargs)
		context["pending_orders"] = Order.objects.filter(order_status="Order Received")
		context["order_completed"] = Order.objects.filter(order_status="Order Completed")
		context["order_ontheway"] = Order.objects.filter(order_status="On the way")
		context["order_processing"] = Order.objects.filter(order_status="Order Processing")
		context["order_canceled"] = Order.objects.filter(order_status="Order Canceled")

		return context


class AdminPendingOrderView(AdminLoginRequireMixin, ListView):
	template_name = "admin/pendingorder.html"
	queryset = Order.objects.filter(order_status="Order Received").order_by('-id')
	context_object_name = "pendings"


class AdminOrderListView(AdminLoginRequireMixin, ListView):
	template_name = "admin/orderlist.html"
	queryset = Order.objects.all().order_by('-id')
	context_object_name = "orderlists"


class AdminOrderStatusChangeView(AdminLoginRequireMixin, View):
	def post(self, request, *args, **kwargs):
		order_id = self.kwargs['pk']
		order_obj = Order.objects.get(id=order_id)
		new_status = request.POST.get("status")
		order_obj.order_status = new_status
		order_obj.save()

		return redirect(reverse_lazy("ecomapp:admin_order_detail", kwargs={"pk":order_id}))

class AdminOrderDetailView(AdminLoginRequireMixin, DetailView):
	template_name = "admin/orderdetail.html"
	model = Order
	context_object_name = "orderdetail"

	def get_context_data(self, *args, **kwargs):
		context = super().get_context_data(**kwargs)
		context['allstatus'] = ORDER_STATUS

		return context

class AdminOrderByStatusView(ListView):
	template_name = 'admin/orderlistbystatus.html'
	queryset = Order.objects.all().order_by('-id')
	context_object_name = "orderlists"

	def get_context_data(self, *args, **kwargs):
		context = super().get_context_data(**kwargs)
		slug = self.kwargs['slug']
		if slug =="Order-Processing":
			slug = "Order Processing"		
		if slug =="On-the-way":
			slug = "On the way"
		if slug =="Order-Completed":
			slug = "Order Completed"
		if slug =="Order-Canceled":
			slug = "Order Canceled"
		if slug =="Order-Received":
			slug = "Order Received"
		context['orderlists'] = Order.objects.filter(order_status=slug)
		context['heading'] = slug

		return context


class AdminProductListView(NonDeletedListMixin, ListView):
	template_name = "admin/productlist.html"
	queryset = Product.objects.all().order_by('-id')
	context_object_name = 'productlist'


class AdminProductDetailView(DetailView):
	template_name = "admin/productdetail.html"
	model = Product
	context_object_name = 'product'

class AdminProductCreateView(CreateView):
	template_name = "admin/productcreate.html"
	form_class = ProductForm
	success_url = reverse_lazy("ecomapp:admin_product_list")

	def get_seller(self):
		seller = Admin.objects.get(username=self.request.user)
		return seller


	def form_valid(self, form):
		print(self.get_seller())
		form.instance.seller = self.get_seller()
		p = form.save()
		images = self.request.FILES.getlist("image")
		print(images, 8888888888888888)
		for i in images:
			print(images, 777777777777777777777)
			form.instance.image = i
		return super().form_valid(form)
	def form_invalid(self, form):
		print(form, 555555555555555555555555)
		return super().form_invalid(form)
class MultipleImageCreateView(CreateView):
	model = Images
	form_class = ImageForm
	template_name = "admin/image.html"

	def dispatch(self, request, *args, **kwargs):
		if self.request.is_ajax():
			return super().dispatch(request, *args, **kwargs)
		return JsonResponse({'error': 'Cannot access this page.'}, status=404)

	def form_valid(self, form):
		instance = form.save()
		return JsonResponse(
			{
				"status": "ok",
				"pk": instance.pk,
				"url": instance.image.url,
			}
		)

class AdminProductUpdateView(UpdateView):
	template_name = "admin/productcreate.html"
	form_class = ProductForm
	model = Product
	success_url = reverse_lazy("ecomapp:admin_product_list")

class AdminProductDeleteView(GetDeleteMixin, DeleteView):
	model = Product
	success_url = reverse_lazy("ecomapp:admin_product_list")

class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        messages.success(request, 'You are logged out successfully.')
        return redirect("/")

class AdminLoginView(FormView):
	template_name = "admin/adminlogin.html"
	form_class = AdminLoginForm
	success_url = reverse_lazy("ecomapp:adminhome")

	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		pword = form.cleaned_data["password"]
		usr = authenticate(username=uname, password=pword)

		if usr is not None and Admin.objects.get(username=uname, is_seller=True):
			login(self.request, usr)
		else:
			return render(self.request, self.template_name, {"form":self.form_class,"error":"Invalid credentials!"})
		return super().form_valid(form)

	def form_invalid(self, form):
		print(form.errors)
		return super().form_invalid(form)


class BaseMixin(object):
    def get_context_data(self, **kwargs):
    	context = super().get_context_data(**kwargs)
    	context['menu_categories'] = Category.objects.all()[:11]

    	return context



class HomeView(BaseMixin, EcomMixin, TemplateView):
	template_name = "home.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		# context['product_lists'] = Product.objects.all()
		allproducts = Product.objects.filter(deleted_at__isnull=True)

		search = self.request.GET.get('search', '')
		
		if search:
			words = search.split(" ")
			if words and len(words)==1:
				allproducts = allproducts.filter(title__icontains=words[0]).distinct()
			elif words and len(words)==2:
				allproducts = allproducts.filter(Q(title__icontains=words[0]) | Q(title__icontains=words[1])).distinct()
			elif words and len(words)==3:
				allproducts = allproducts.filter(Q(title__icontains=words[0]) | Q(title__icontains=words[1]) | Q(title__icontains=words[2])).distinct()
			elif words and len(words)==4:
				allproducts = allproducts.filter(Q(title__icontains=words[0]) | Q(title__icontains=words[1]) | Q(title__icontains=words[2]) | Q(title__icontains=words[3])).distinct()


		paginator = Paginator(allproducts, 3)
		page_number = self.request.GET.get('page')
		product_lists = paginator.get_page(page_number)
		context['product_lists'] =product_lists

		return context



class SearchView(BaseMixin, TemplateView):
	template_name = "search.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		kw = self.request.GET['keyword']
		results = Product.objects.filter(Q(title__icontains=kw) | Q(description__icontains=kw))
		context['results'] = results

		return context


class AllProductsView(BaseMixin, EcomMixin, TemplateView):
	template_name = 'allproducts.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['allcategories'] = Category.objects.all()

		return context



class ProductDetailView(BaseMixin, EcomMixin, TemplateView):
	template_name = 'productdetail.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		url_slug = self.kwargs['slug']
		product = Product.objects.get(slug=url_slug)
		product.view_count += 1
		product.save()

		context['product'] = product

		return  context

class AddToCartView(BaseMixin, EcomMixin, View):
	# template_name = 'addtocart.html'

	def get(self, *args, **kwargs):
		product_id = self.kwargs['pro_id']
		product_obj = Product.objects.get(id=product_id)
		#check if wether cart exist or not
		cart_id = self.request.session.get('cart_id', None)
		if cart_id:
			cart_obj = Cart.objects.get(id=cart_id)
			this_product_in_cart = cart_obj.cartproduct_set.filter(product=product_obj)
			if this_product_in_cart.exists():
				cartproduct = this_product_in_cart.last()
				cartproduct.quantity +=1
				cartproduct.subtotal +=product_obj.selling_price
				cartproduct.save()
				cart_obj.total +=product_obj.selling_price
				cart_obj.save()


			#new item is added
			else:
				cartproduct = CartProduct.objects.create(cart=cart_obj,product=product_obj, rate=product_obj.selling_price,quantity=1,subtotal=product_obj.selling_price)
				cart_obj.total +=product_obj.selling_price
				cart_obj.save() 
		else:
			cart_obj = Cart.objects.create(total=0)
			self.request.session['cart_id'] = cart_obj.id
			cartproduct = CartProduct.objects.create(cart=cart_obj,product=product_obj, rate=product_obj.selling_price,quantity=1,subtotal=product_obj.selling_price)
			cart_obj.total +=product_obj.selling_price
			cart_obj.save()
		messages.success(self.request, 'Item added to cart.')
		return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class MycartView(BaseMixin, EcomMixin, CreateView):
	template_name = 'mycart.html'
	form_class 		= CheckOutForm
	success_url 	= reverse_lazy('ecomapp:home')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		cart_id = self.request.session.get("cart_id", None)
		if cart_id:
			cart = Cart.objects.get(id=cart_id)
		else:
			cart = None
		context['cart'] = cart

		return context


class ManageCartView(BaseMixin, EcomMixin, TemplateView):
	def get(self, request, *args, **kwargs):
		cp_id = self.kwargs['cp_id']
		action = request.GET.get('action')
		quantity = int(self.request.GET.get('quantity'))
		cp_obj = CartProduct.objects.get(id=cp_id)
		cart_obj = cp_obj.cart
		if quantity:
			cp_obj.quantity = (quantity)
			cp_obj.subtotal = cp_obj.rate * quantity
			cp_obj.save(update_fields=['quantity', 'subtotal'])
			cart_obj.total = cp_obj.rate * quantity
			cart_obj.save(update_fields=['total'])
			if cp_obj.quantity == 0:
				cp_obj.delete()
			return JsonResponse({'subtotal': cp_obj.subtotal, 'total' : cart_obj.total})

		if action == "rmv":
			cart_obj.total -=cp_obj.subtotal
			cart_obj.save(update_fields=['total'])
			cp_obj.delete()
		return redirect('ecomapp:mycart')
		


class EmptyCartView(EcomMixin, View):
	def get(self, request, *args, **kwargs):
		cart_id = request.session.get('cart_id', None)
		if cart_id:
			cart = Cart.objects.get(id=cart_id)
			cart.cartproduct_set.all().delete()
			cart.total = 0
			cart.save()

		return redirect('ecomapp:mycart')


class CheckOutView(BaseMixin, EcomMixin, CreateView):
	template_name	= 'checkout.html'
	form_class 		= CheckOutForm
	success_url 	= reverse_lazy('ecomapp:home')

	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and request.user.groups.filter(name="Seller"):
			pass
		else:
			return redirect('/c-login/?next=/check-out/')

		return super().dispatch(request, *args, **kwargs)


	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		cart_id = self.request.session.get("cart_id", None)
		if cart_id:
			cart = Cart.objects.get(id=cart_id)
		else:
			cart = None
		context['cart'] = cart

		return context


	def form_valid(self, form):
		cart_id = self.request.session.get('cart_id')
		if cart_id:
			cart_obj = Cart.objects.get(id=cart_id)
			form.instance.cart = cart_obj
			form.instance.subtotal = cart_obj.total
			form.instance.discount = 0
			form.instance.total = cart_obj.total
			form.instance.order_status = "Order-Received"
			del self.request.session['cart_id']
			pm = form.cleaned_data.get("payment_method")
			order = form.save()
			if pm == "khalti":
				return redirect(reverse("ecomapp:khaltirequest"))
		else:
			return redirect("ecomapp:home")
		return super().form_valid(form)


class KhaltiRequestView(View):
	def get(self, request, *args, **kwargs):
		context = {

		}
		return render(request, "khaltirequest.html", context)

class CustomerRegistrationView(BaseMixin, CreateView):
	template_name 	= "customerregistration.html"
	form_class 		= CustomerRegistrationForm
	success_url 	= reverse_lazy('ecomapp:home')

	def form_valid(self, form):
		username = form.cleaned_data.get('email')
		password = form.cleaned_data.get('password')
		user = User.objects.create(username=username)
		print(user, 8888888888888888888, username, password)
		user.set_password(password)
		print(user.set_password(password), 77777777777777777)
		user.save(update_fields=['password'])
		form.instance.user = user
		return super().form_valid(form)

	def get_success_url(self):
		if "next" in self.request.GET:
			next_url = self.request.GET.get("next")
			return next_url
		else:
			return self.success_url


class UserNameCheckerView(BaseMixin, EcomMixin, View):
    def get(self, request):
        myname = request.GET.get("my_user_name")
        if User.objects.filter(username=myname).exists():
            message = "  User already exist, try another!"
            color = "red"
        else:
            message = "  Available"
            color = "blue"
        return JsonResponse({
            "msg_text": message,
            "color": color,
        })


class CustomerLogoutView(EcomMixin, View):
	def get(self, request):
		logout(request)
		return redirect('ecomapp:home')


class CustomerLoginView(EcomMixin, View):
	def post(self, request, *args, **kwargs):
		name = request.POST['name']
		pword = request.POST['password']
		usr = authenticate(username=name, password=pword)
		if usr is not None and Customer.objects.get(username=name, is_customer=True).exists():
			login(self.request, usr)
		else:
			errormsg = "Your user name or password is wrong!"
			product_lists = Product.objects.all()
			return render(request, 'home.html', {'errormsg':errormsg,'product_lists':product_lists})

		return redirect('ecomapp:home')




class CutomLoginView(BaseMixin, FormView):
	template_name 	= "customerlogin.html"
	form_class 		= CustomerLoginForm
	success_url 	= reverse_lazy('ecomapp:home')

	def form_valid(self, form):
		name = form.cleaned_data['username']
		pword = form.cleaned_data['password']
		usr = authenticate(username=name, password=pword)
		if usr is not None and Customer.objects.get(user=usr, is_customer=True):
			login(self.request, usr)
		else:
			return render(self.request, self.template_name, {'form':self.form_class,'error':'Invalid user Credentials!'})


		return super().form_valid(form)


	def get_success_url(self):
		if "next" in self.request.GET:
			next_url = self.request.GET.get("next")
			return next_url
		else:
			return self.success_url



class CustomerProfileView(BaseMixin, EcomMixin, TemplateView):
	template_name = "customerprofile.html"


	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and Customer.objects.filter(user=request.user).exists():
			pass
		else:
			return redirect('/c-login/?next=/customer-profile/')

		return super().dispatch(request, *args, **kwargs)

	def get_context_data(self, *args, **kwargs):
		context = super().get_context_data(**kwargs)
		customer = self.request.user.customer
		context['customer'] = customer
		orders = Order.objects.filter(cart__customer=customer).order_by('-id')
		context['orders'] = orders

		return context


class CustomerOrderDetailView(BaseMixin, EcomMixin, DetailView):
	template_name = "customerorderdetail.html"
	model = Order
	context_object_name = "orderdetail"

	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and Customer.objects.filter(user=request.user).exists():
			order_id = self.kwargs['pk']
			order = Order.objects.get(id=order_id)
			if request.user.customer != order.cart.customer:
				return redirect("/customer-profile/")
		else:
			return redirect('/c-login/?next=/customer-profile/')

		return super().dispatch(request, *args, **kwargs)


class ForgotPasswordView(FormView):
	template_name = 'forgotpassword.html'
	form_class = ForgotPasswordForm
	success_url = "/forgot-password/?m=s"

	def form_valid(self, form):
		email = form.cleaned_data.get("email")
		url = self.request.META["HTTP_HOST"]
		customer = User.objects.get(email = email)
		# user = customer.email
		text_content = "Please click the link below to reset your password"
		html_content = ' http://'+ url + "/password-reset/" + email + \
			"/" + password_reset_token.make_token(customer) + "/"
		send_mail(
			"Password Reset Link | Sajilobazar",
			text_content+html_content,
			settings.EMAIL_HOST_USER,
			[email],
			fail_silently=False,


		)
		return super().form_valid(form)
		
class PasswordResetView(FormView):
	template_name = "passwordreset.html"
	form_class = PasswordResetForm
	success_url = "/c-login/"


	def dispatch(self, request, *args, **kwargs):
		email = self.kwargs.get("email")
		user = User.objects.get(email=email)
		token = self.kwargs.get("token")
		if user is not None and password_reset_token.check_token(user, token):
			pass
		else:
			return redirect(reverse("ecomapp:forgot_password")+"?m=e")

		return super().dispatch(request, *args, **kwargs)


	def form_valid(self, form):
		password = form.cleaned_data['new_password']
		email = self.kwargs.get("email")
		user = User.objects.get(email=email)
		user.set_password(password)
		user.save()

		return super().form_valid(form)



class CategorisedProductView(BaseMixin, TemplateView):
	template_name = "categorisedprduct.html"

	def get_context_data(request, *args, **kwargs):
		context = super().get_context_data(**kwargs)
		slug = request.kwargs['slug']
		cat = Category.objects.get(slug=slug)
		context['products'] = Product.objects.filter(category=cat)
		context['cat'] = cat

		return context





class AboutView(BaseMixin, EcomMixin, TemplateView):
	template_name = 'about.html'


class ContactView(BaseMixin, EcomMixin, TemplateView):
	template_name = 'contact.html'


#Review view
class ProductReview(TemplateView):
	template_name = "productreview.html"

	def get_prodect(self):
		if not hasattr(self, 'product'):
			self.product = get_object_or_404(Product, pk=self.kwargs.get('product_pk'))
		return self.product
	
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['product'] = self.get_prodect()
		return context

	def post(self, *args, **kwargs):
		user = Customer.objects.filter(user=self.request.user)
		print(user, 8888888888888888)
		if self.request.is_ajax():
			for qs in user:
				print(qs, 44444444444444444)
				description = self.request.POST.get('review')
				ratings = self.request.POST.get('ratings')
				Review.objects.create(user = qs, product = self.get_prodect(), rate=ratings, description=description)
				messages.success(self.request, 'thank you for your feedback')
		return JsonResponse({'url': reverse_lazy('ecomapp:product-detail', kwargs={'slug':self.get_prodect().slug})}, status=200)
			

